package info.scce.dime.mobile.generator.mad

import de.jabc.cinco.meta.plugin.generator.runtime.IGenerator
import de.jabc.cinco.meta.runtime.CincoRuntimeBaseClass
import graphmodel.IdentifiableElement
import info.scce.dime.mobile.mad.modelid.mad.MAD
import info.scce.dime.mobile.mad.modelid.mad.ProcessComponent
import info.scce.dime.process.process.GUISIB
import info.scce.dime.process.process.Process
import info.scce.dime.process.process.ProcessSIB
import org.eclipse.core.runtime.IPath
import org.eclipse.core.runtime.IProgressMonitor

/**
 * Generator that generates sources from DIME models.
 */
class Generator extends CincoRuntimeBaseClass implements IGenerator<MAD> {
	
	override generate(MAD mad, IPath path, IProgressMonitor monitor) {
		// project holding the MAD file
		val project = mad.project
		// pretend that generation takes some time
		Thread.sleep(3000)
		// target folder for generated files
		val outlet = project.createFolder("target/mobile-app/processes")
		// generate a file for each Process model reachable from the MAD model
		mad.findDeeply(Process, [primeReferencedModels]).forEach[
			outlet.createFile('''«modelName».java''', fileContent.toString)
		]
	}
	
	def getFileContent(Process process) '''
		public class «process.modelName» {
			// TODO
		}
	'''
	
	/**
	 * Utility function that maps nodes with prime references on
	 * the model that is referenced.
	 */
	def getPrimeReferencedModels(IdentifiableElement it) {
		switch it {
	   		ProcessComponent: model as Process
			ProcessSIB: proMod
			GUISIB: gui
			info.scce.dime.gui.gui.GUISIB: gui
			info.scce.dime.gui.gui.ProcessSIB: proMod as Process
		}
	}
}
