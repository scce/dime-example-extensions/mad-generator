# DIME Example: Mobile Application Generator

An example to illustrate how to extend DIME by means of an additional model type that brings in its own generator.

## Requirements

This example is supposed to be run in DIME.
Using the DIME installer is recommended.

## Approach

The goal pursued in this example is to generate a mobile application from DIME models.
The chosen approach is to create a graphical MAD model (Mobile Application Descriptor) with a CINCO Product Project and register a corresponding generator.
While the example abstracts from the actual code to be generated it shows how to

- create a new model type with Prime References on DIME models
- register and implement a generator
- register and implement validation routines

## Build the Example Project
* Clone the Git repository of the example app
* Run DIME with a new workspace
* Import the bundles from the example app repository
    * From the menu select _File_ -> _Import..._ -> _General_ -> _Existing Projects into Workspace_
    * Select the cloned example app Git repository as _root directory_
    * Select all projects and hit _Finsh_
* Right-Click `info.scce.dime.mobile/model/MAD.cpd` and run _Generate Cinco Product_

## Modify the MAD Generator
* The generator is registered by the `@generatable` annotation in `info.scce.dime.mobile/model/MAD.mgl`
* Find the `Generator.xtend` file in `info.scce.dime.mobile.generator/src`

## Run the Example Project
* Right-Click on a project in the Project Explorer and run _Run As_ -> _Eclipse Application_
* In the new DIME instance
    * Make sure that the DIME perspective is active, or open it via menu _Window_ -> _Perspective_ -> _Open Perspective_
    * Right-Click in the Project-Explorer and select _New_ -> _New DIME App_
    * Choose the option _Create a new application from scratch_ and hit _Finish_

## Run the MAD Generator
* Create a new MAD model
    * Right-Click the folder `dime-models`, select _New_ -> _New MAD_ and give it a name (e.g. 'app')
    * Open the `.mad` file if not already open
    * Create nodes in the MAD model just like you would do in a DAD model
* With the MAD model opened, hit the (G)enerate button in the toolbar
* Find the generated files in `target/mobile-app`
