package info.scce.dime.mobile.mad.checks

import info.scce.dime.mobile.mad.modelid.mad.MAD
import info.scce.dime.mobile.mad.modelid.mad.ProcessComponent
import info.scce.dime.mobile.mad.modelid.mad.RootInteractionPointer
import info.scce.dime.mobile.mad.modelid.mad.Start
import info.scce.dime.mobile.mad.modelid.mad.StartupProcessPointer
import info.scce.dime.mobile.mad.modelid.mcam.modules.checks.MADCheck
import info.scce.dime.process.process.Process

import static info.scce.dime.process.process.ProcessType.*

class CorrectnessCheck extends MADCheck {
	
	override check(MAD mad) {
		if (mad.starts.size != 1)
			mad.addError("exactly one Start required")
		else mad.starts.head.check
		
		if (mad.systemUsers.size != 1)
			mad.addError("exactly one SystemUser required")
	}
	
	def check(Start start) {
		if (start.getOutgoing(RootInteractionPointer).size != 1)
			start.addError("exactly one root interaction required")
		else {
			val target = start.findTargetOf(RootInteractionPointer) as ProcessComponent
			val process = target.model as Process
			if (process.processType != BASIC)
				target.addError("root interaction must be process of type BASIC")
		}
		
		if (start.getOutgoing(StartupProcessPointer).size > 1)
			start.addError("only one startup process allowed")
		else {
			val target = start.findTargetOf(StartupProcessPointer) as ProcessComponent
			if (target !== null) {
				val process = target.model as Process
				if (process.processType != BASIC)
					target.addError("startup process must be process of type BASIC")
			}
		}
	}
	
}